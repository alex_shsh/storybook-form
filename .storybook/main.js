// https://gist.github.com/shilman/bc9cbedb2a7efb5ec6710337cbd20c0c
// https://blog.crowdbotics.com/setup-a-react-app-with-typescript-storybook-and-crowdbotics/
module.exports = {
  stories: ["../src/stories/**/*.stories.@(ts|tsx|js|jsx|mdx)"],
  addons: [
    "@storybook/addon-actions",
    "@storybook/addon-links",
    "@storybook/preset-create-react-app",
    {
      name: "@storybook/addon-docs",
      options: {
        configureJSX: true,
      },
    },
  ], 
};