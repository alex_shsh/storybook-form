import React from 'react';
import SignUp from './views/SignUp/SignUp';

function App() {
  return (
    <div className="App">
      <SignUp />
    </div>
  );
}

export default App;
