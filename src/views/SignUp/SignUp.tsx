import React, { useState } from 'react';
import { Form } from '../../stories/Form/Form';
import { TextInput } from '../../stories/TextInput/TextInput';
import { Select } from '../../stories/Select/Select';
import { Radio } from '../../stories/Radio/Radio';
import { Checkbox } from '../../stories/Checkbox/Checkbox';
import style from './SignUp.module.css'
import { Button } from '../../stories/Button/Button';
import BlockWindow from './BlockWindow';
// assets
import { ReactComponent as EmailSvg }from '../../assets/email.svg'
import { ReactComponent as PasswordSvg }from '../../assets/password.svg'
import { getErrorText } from './getError';

interface State {
  name: string;
  email: string;
  password: string;
  country: string;
  gender: string;
  agree: boolean;
  [key: string]: boolean | string;
}

const initial = {
  name: '',
  email: '',
  password: '',
  country: '',
  gender: '',
  agree: false,
}

const counties = [
  {value: 'Latvia', label: 'Latvia'}, 
  {value: 'Lebanon', label: 'Lebanon'}, 
  {value: 'Lesotho', label: 'Lesotho'}
]

const genders = [
  {value: 'MALE', label: 'Male'},  
  {value: 'FEMALE', label: 'Female'},  
]
   
/** Component page */
const SignUp = () => {
  const [state, setstate] = useState<State>(initial);
  const [errors, setErrors] = useState<{[key: string]: string}>({});
  const [isValidate, setIsValidate] = useState(false);
  const [loading, isLoading] = useState(false);

  function onChange(nameField: string) {
    return (e: React.ChangeEvent<HTMLInputElement>) => {
      e.persist() 
      const value = e.target.value
      handleError(nameField, value)
      setstate(prev => ({...prev, [nameField]: value}))
    }
  } 

  function onChangeSelect(country: string) {
    setstate(prev => ({...prev, country}))
  }

  function onChangeCheckbox(agree: boolean) {
    setstate(prev => ({...prev, agree}))
  }

  function handleError(nameField: string, value: string) {
    let resultTextError = ''
    if(value.trim().length > 0){
      const fieldErrorText = getErrorText(nameField, value)
      if(fieldErrorText) resultTextError = fieldErrorText
      else resultTextError = ''
    } else {
      resultTextError = ''
    }
    setErrors(prev => ({...prev, [nameField]: resultTextError}))
  }
 
  /** Final */
  function onSubmit(e: React.FormEvent<HTMLFormElement> | React.FormEvent<HTMLInputElement>) { 
    e.preventDefault()
    isLoading(true)
    new Promise((resolve) => 
      setTimeout(() => {
        resolve({errors: {}})
      }, 10000)
    ).then((res: any) => { 
      isLoading(false)
      if(res.errors.length) setErrors(res.errors)
    }).catch(() => isLoading(false))
  }
 
  /** Validation at the start and each state update */
  React.useEffect(() => {  
    let isValid = true
    for (const key in state) {
      const errorText = getErrorText(key, state[key])  
      if(errorText) {
        setErrors(prev => ({...prev, [key]: errorText})) 
        isValid = false
      }
      else setErrors(prev => ({...prev, [key]: ''})) 
    } 
    setIsValidate(isValid) 
  }, [state])
 
  return (
    <div className={style['SignUp']}>
      <Form className={style['SignUp-form']} onSubmit={onSubmit}>
        <h1 className={style['SignUp-h1']}>Create a new account</h1>
        <TextInput value={state['name']} onChange={onChange('name')} errorText={errors['name']}  
          placeholder={'Enter your name'}
        />
        <TextInput value={state['email']} onChange={onChange('email')} errorText={errors['email']} 
          icon={<EmailSvg />}
          placeholder={'Email'}
        />
        <TextInput value={state['password']} onChange={onChange('password')} errorText={errors['password']} 
          icon={<PasswordSvg />} isPassword
          placeholder={'Password'}
        />
        <Select value={state['country']} onChange={onChangeSelect} errorText={errors['country']}
          options={counties}
          placeholder={'Select country'}
        /> 
        <Radio options={genders} onChange={onChange('gender')} currentValue={state['gender']} 
          errorText={errors['gender']}
        /> 
        <Checkbox value={state['agree']} onChange={onChangeCheckbox} 
          errorText={errors['agree']} className={style['SignUp-checkbox']}
        >
          Accept <a href="/">terms</a> and <a href="/">conditions</a>
        </Checkbox>
        <Button text={'Sign up'} onSubmit={onSubmit} allowed={!loading && isValidate} loading={loading} />

        <BlockWindow loading={loading} />
      </Form>
    </div>
  );
}

export default SignUp;
