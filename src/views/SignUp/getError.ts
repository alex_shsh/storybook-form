export function getErrorText(nameField: string, value: string | boolean): string {
  let validate: {[key: string]: () => string} = {}

  if(typeof value === 'string') {
    const val = value.trim() 
    validate = { 
      'name': () => /^[a-zA-Z]+$/.test(val) && val.length >= 2 ? '' : 'Please enter a valid name',
      'email': () => /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(val) ? '' : 'Please enter a valid email address',
      'password': () => val.length >= 6 ? '' : 'Password must contain at least 6 symbols',
      'country': () => val.length > 1 ? '' : 'You must select your country',
      'gender': () => value ? '' : 'You must select your country',
    }
  } else { 
    validate = {  
      'agree': () => value ? '' : 'You must accept the policies',
    }
  }
   
  return validate[nameField] ? validate[nameField]() : ''
}