import React from 'react';

const BlockWindow: React.FC<{loading: boolean}> = ({loading = false}) => {
  return (
    <div style={{
      zIndex: 10, 
      background: 'transparent', 
      width: '100%',
      height: '100%',
      position: 'absolute',
      top: '0',
      left: '0',
      display: loading ? 'block' : 'none'}}
    >
      
    </div>
  );
}

export default BlockWindow;
