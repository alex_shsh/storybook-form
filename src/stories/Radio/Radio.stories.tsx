import { Meta, Story } from '@storybook/react/types-6-0';
import React from 'react';
import {Radio, Props} from './Radio';

 
export default {
  title: 'Radio',
  component: Radio, 
}  as Meta ;


const Template: Story<Props> = (args: Props) => <Radio {...args} />;

export const Primary = Template.bind({});
Primary.args = { 
 
};
