import React from 'react';
import style from './Radio.module.css'

interface Option{ 
  value: any, label: string
}

export interface Props { 
  currentValue: string;
  onChange: (selected: any) => void;
  options: Option[]
 
  className?: string;
  errorText?: string;
}

export const Radio: React.FC<Props> = ({className, options, currentValue, onChange, errorText}) => { 

  return (
    <div className={style['uiRadio'] + `${className ? ' '+className : ''}`}>
      {options && options.length && options.map(el => 
        <label  
          key={el.value}
          className={style['uiRadio-one']} 
        >
          <span className={style['uiRadio-circle'] + `${currentValue === el.value ? ' '+style['uiRadio-circle--selected'] : ''}`} />
          <input type="radio" value={el.value} checked={currentValue === el.value} onChange={onChange} />
          {el.label && <span>{el.label}</span>}
        </label>
      )}

      {errorText && <div className={style['uiRadio-error']}>{errorText}</div>}
    </div>
  )
}
 