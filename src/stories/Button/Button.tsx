import React from 'react';
import style from './Button.module.css'
 

export interface Props {  
  onSubmit?: (e: React.FormEvent<HTMLInputElement>) => void;
  allowed?: boolean;
  loading?: boolean;
  
  text?: string;
}

export const Button: React.FC<Props> = ({text, onSubmit, allowed = true, loading = false}) => { 

  return ( 
    <div className={style['uiButton-wrap']}>
      <input type="submit" value={text && !loading ? text : ''} 
        onSubmit={allowed ? onSubmit : undefined}
        className={style['uiButton'] + `${allowed ? ' '+style['uiButton-active'] : ''}`} 
      />
      {loading && <div className={style['uiButton-loading']}></div>}
    </div> 
  )
}
 