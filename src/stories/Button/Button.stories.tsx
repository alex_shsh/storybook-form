import { Meta, Story } from '@storybook/react/types-6-0';
import React from 'react';
import {Button, Props} from './Button';

 
export default {
  title: 'Button',
  component: Button, 
}  as Meta ;


const Template: Story<Props> = (args: Props) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = { 
 
};
