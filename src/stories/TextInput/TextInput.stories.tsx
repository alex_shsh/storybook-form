import { Meta, Story } from '@storybook/react/types-6-0';
import React from 'react';
import {TextInput, Props} from './TextInput';

 
export default {
  title: 'TextInput',
  component: TextInput, 
}  as Meta ;


const Template: Story<Props> = (args: Props) => <TextInput {...args} />;

export const Primary = Template.bind({});
Primary.args = {  };
