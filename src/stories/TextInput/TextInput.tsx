import React from 'react';
import style from './TextInput.module.css'

export interface Props {
  value: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;

  errorText?: string | null;
  icon?: any;
  isPassword?: boolean;
  placeholder?: string; 
}

export const TextInput: React.FC<Props> = ({value, onChange, icon, errorText, placeholder, isPassword}) => { 
  

  return (
    <div 
      className={style['uiTextInput']} 
    > 
      {icon && <div className={style['uiTextInput-icon']}>{icon}</div>}

      <input type={isPassword ? 'password' : "text" } className={style['uiTextInput-input']}
        value={value} onChange={onChange} placeholder={placeholder}
      />

      {errorText && <div className={style['uiTextInput-error']}>{errorText}</div>}
    </div>
  )
}
 