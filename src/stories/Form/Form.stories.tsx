import { Meta, Story } from '@storybook/react/types-6-0';
import React from 'react';
import {Form, Props} from './Form';

 
export default {
  title: 'Form',
  component: Form, 
}  as Meta ;


const Template: Story<Props> = (args: Props) => <Form {...args} />;

export const Primary = Template.bind({});
Primary.args = { 
  color: '#e7e7e7',
};
