import React from 'react';
import style from './Form.module.css'

export interface Props {
  color?: string;
  className?: string;
  onSubmit: (e: React.FormEvent<HTMLFormElement>) => void;
}

export const Form: React.FC<Props> = ({color = '#fff', onSubmit, className, children}) => { 

  return (
    <form 
      onSubmit={onSubmit}
      className={style['ui-form'] + `${className ? ' '+className : ''}`}
      style={{backgroundColor: color}}
    >{children}</form>
  )
}
 