import { Meta, Story } from '@storybook/react/types-6-0';
import React from 'react';
import {Checkbox, Props} from './Checkbox';

 
export default {
  title: 'Checkbox',
  component: Checkbox, 
}  as Meta ;


const Template: Story<Props> = (args: Props) => <Checkbox {...args} />;

export const Primary = Template.bind({});
Primary.args = { 
 
};
