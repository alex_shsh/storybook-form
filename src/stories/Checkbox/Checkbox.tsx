import React from 'react';
import style from './Checkbox.module.css'
 

export interface Props {  
  onChange: (selected: any) => void;
  value: boolean;
  
  children: React.ReactNode;
  className?: string;
  errorText?: string;
}

export const Checkbox: React.FC<Props> = ({className, value, children, onChange, errorText}) => { 

  return (
    <div className={style['uiCheckbox'] + `${className ? ' '+className : ''}`}> 
      <div 
        onClick={onChange.bind(null, !value)} 
        className={style['uiCheckbox-box']}
      >{value && <span>&#10003;</span>}</div>

      {children && <span className={style['uiCheckbox-text']}>{children}</span>}
      {errorText && <div className={style['uiCheckbox-error']}>{errorText}</div>}
    </div>
  )
}
 