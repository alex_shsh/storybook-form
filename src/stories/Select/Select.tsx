import React, {useState, useEffect} from 'react';
import style from './Select.module.css' 
import { ReactComponent as ArrowSvg } from './arrow.svg' 

interface Option {
  value: any, label: string
}

export interface Props {
  value: string;
  onChange: (selected: any) => void;
  options: Option[]

  placeholder?: string;
  errorText?: string | null; 
}

export const Select: React.FC<Props> = ({value, onChange, errorText, placeholder, options}) => {
  const [opened, setOpened] = useState(false); 
  const [text, setText] = useState(''); 
 
  useEffect(() => { 
    setText(value)
  }, [value]);

  function handleSelectClick() {  
    if(text !== value) setText(value)
    setOpened(prev => !prev)
  }

  function handleItemClick(e: React.MouseEvent<HTMLLIElement, MouseEvent>, item: string) {
    if(text !== value) setText(value)
    e.stopPropagation()
    onChange(item)
    setOpened(false)
  }

  function handleInputText(e: React.ChangeEvent<HTMLInputElement>) { 
    e.persist()
    setText(e.target.value)
  }
 
  return (
    <div 
      className={style['uiSelect']} 
      onClick={handleSelectClick}
    >   
      <input type="text" className={style['uiSelect-input']}
        value={text} onChange={handleInputText} placeholder={placeholder}
      /> 
      <ArrowSvg className={style['uiSelect-arrow']} />

      {opened && <ul className={style['uiSelect-list']}>
        {options && options.length && options.map((el: Option) => {
          if(text !== value && !el.label.includes(text)) return null

          return <li 
            key={el.value}
            onClick={(e) => handleItemClick(e, el.value)}
            className={style['uiSelect-list-li'] + `${value === el.value ? ' '+style['uiSelect-list-li--selected'] : ''}`}
          >
            {el.value}
          </li>
        })}
      </ul>}

      {errorText && <div className={style['uiSelect-error']}>{errorText}</div>}
    </div>
  )
}
 