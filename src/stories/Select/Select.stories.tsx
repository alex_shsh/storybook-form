import { Meta, Story } from '@storybook/react/types-6-0';
import React from 'react';
import {Select, Props} from './Select';

 
export default {
  title: 'Select',
  component: Select, 
}  as Meta ;


const Template: Story<Props> = (args: Props) => <Select {...args} />;

export const Primary = Template.bind({});
Primary.args = {  };
